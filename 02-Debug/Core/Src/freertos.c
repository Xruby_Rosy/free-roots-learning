/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usart.h"
#include<string.h>
#include"stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;  //声明
/* USER CODE END Variables */
osThreadId CPU_TaskHandle;
osThreadId myTask02Handle;
osThreadId myTask03Handle;
osThreadId myTask04Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void CPURunInfo_Task(void const * argument);
void StartTask02(void const * argument);
void StartTask03(void const * argument);
void StartTask04(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime =0;  //系统调度的时候进行清零
}

__weak unsigned long getRunTimeCounterValue(void)
{
	return CPU_RunTime;   //此处是获取CPU_RunTime这个值，故进行返回就可以
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of CPU_Task */
  osThreadDef(CPU_Task, CPURunInfo_Task, osPriorityAboveNormal, 0, 256);
  CPU_TaskHandle = osThreadCreate(osThread(CPU_Task), NULL);

  /* definition and creation of myTask02 */
  osThreadDef(myTask02, StartTask02, osPriorityBelowNormal, 0, 128);
  myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of myTask03 */
  osThreadDef(myTask03, StartTask03, osPriorityBelowNormal, 0, 128);
  myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* definition and creation of myTask04 */
  osThreadDef(myTask04, StartTask04, osPriorityBelowNormal, 0, 128);
  myTask04Handle = osThreadCreate(osThread(myTask04), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_CPURunInfo_Task */
/**
  * @brief  Function implementing the CPU_Task thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_CPURunInfo_Task */
void CPURunInfo_Task(void const * argument)
{
  /* USER CODE BEGIN CPURunInfo_Task */
	char CPU_RunInfo[256];
	char buffer0[100]=" Name       State    Priority LeftStack     Number  \r\n";
	char buffer1[100]=" Name       RunCount      UtilizeRatio  \r\n";
  /* Infinite loop */
  for(;;)
  {
	  vTaskList(CPU_RunInfo);  //获取任务信息
	  HAL_UART_Transmit(&huart2,(uint8_t*)"HAL_TIM_Base_Start_IT \r\n", 20, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer0 , strlen(buffer0), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	  HAL_UART_Transmit(&huart2, (uint8_t*)"-------------------- \r\n", 23, HAL_MAX_DELAY);
	  osDelay(1000);
  }
  /* USER CODE END CPURunInfo_Task */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(LED_BULE_GPIO_Port, LED_BULE_Pin);
	  osDelay(500);
  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_StartTask03 */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask03 */
void StartTask03(void const * argument)
{
  /* USER CODE BEGIN StartTask03 */
  /* Infinite loop */
  for(;;)
  {
	HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
	osDelay(1000);

  }
  /* USER CODE END StartTask03 */
}

/* USER CODE BEGIN Header_StartTask04 */
/**
* @brief Function implementing the myTask04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask04 */
void StartTask04(void const * argument)
{
  /* USER CODE BEGIN StartTask04 */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
	  osDelay(2000);

  }
  /* USER CODE END StartTask04 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

