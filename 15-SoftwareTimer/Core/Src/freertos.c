/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"KEY.h"
#include"usart.h"
#include<stdio.h>
#include<string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
extern volatile uint32_t CPU_RunTime;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
char buff[100];
//定义ID号
uint8_t Timer01_ID =1;
uint8_t Timer02_ID =2;

//定义软件定时器句柄
static TimerHandle_t MyTimer01Handle = NULL;
static TimerHandle_t MyTimer02Handle = NULL;

/* USER CODE END Variables */
osThreadId LED_BHandle;
osThreadId KEYHandle;
osThreadId LED_GHandle;
osThreadId LED_RHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void LED_B_Task(void const * argument);
void KEY_Task(void const * argument);
void LED_G_Task(void const * argument);
void LED_R_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
static void vMyTimerCallback(xTimerHandle pxTimer)  //定时器回调函数
{
	uint8_t Timer_ID =0;//软件定时器ID
	static uint16_t Timer01CallBackCnt = 0;
	static uint16_t Timer02CallBackCnt = 0;

	//校验形参
	configASSERT(pxTimer);
	//获取软件定时器ID
	Timer_ID = *((uint8_t*)pvTimerGetTimerID(pxTimer));

	//执行软件定时器1任务
	if(Timer_ID == Timer01_ID)
	{
		HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
		sprintf(buff,"软件定时器1回调次数= %u\r\n",++Timer01CallBackCnt);
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	}

	//执行软件定时器2任务
	if(Timer_ID == Timer02_ID)
	{
		//软件定时器2任务...
		HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
		sprintf(buff,"软件定时器2回调次数= %u\r\n",++Timer02CallBackCnt);
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	}
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
//创建软件定时器1并启动
	MyTimer01Handle = xTimerCreate(
			"Timer01",   //名字
			100,         //定时周期 100ms
			pdTRUE,      //周期模式
			(void *)&Timer01_ID, //ID
			vMyTimerCallback //回调函数
	);
	if(MyTimer01Handle != NULL)
	{
		sprintf(buff,"%s \r\n","创建软件定时器1成功");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);

		if(xTimerStart(MyTimer01Handle,0) == pdPASS)
		{
			sprintf(buff,"%s \r\n","启动软件定时器1成功");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
	}

	MyTimer02Handle = xTimerCreate(
			"Timer02",//名字
			500,//定时周期 500ms
			pdTRUE, //周期模式
			(void *)&Timer02_ID, //ID
			vMyTimerCallback //回调函数
	);
	if(MyTimer02Handle != NULL)
	{
		sprintf(buff,"%s \r\n","创建软件定时器2成功");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);

		if(xTimerStart(MyTimer02Handle,0) == pdPASS)
		{
			sprintf(buff,"%s \r\n","启动软件定时器2成功");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
	}
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of LED_B */
  osThreadDef(LED_B, LED_B_Task, osPriorityBelowNormal, 0, 128);
  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of LED_G */
  osThreadDef(LED_G, LED_G_Task, osPriorityBelowNormal, 0, 128);
  LED_GHandle = osThreadCreate(osThread(LED_G), NULL);

  /* definition and creation of LED_R */
  osThreadDef(LED_R, LED_R_Task, osPriorityBelowNormal, 0, 128);
  LED_RHandle = osThreadCreate(osThread(LED_R), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_LED_B_Task */
/**
  * @brief  Function implementing the LED_B thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_LED_B_Task */
void LED_B_Task(void const * argument)
{
  /* USER CODE BEGIN LED_B_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END LED_B_Task */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
//	char CPU_RunInfo[500];//保存任务运行时间信息
//	char buffer1[100]="任务名       任务状态       优先级   剩余栈    任务序号 \r\n  ";
//	char buffer2[100]="任务名       运行计数       利用率\r\n";
//  /* Infinite loop */
  for(;;)
  {
  //按键检测
	  KEY.GetKeyCode();
	//KEY1处理
	if(KEY.KeyCode==KEY1){

//		sprintf(buff,"%s \r\n","当前剩余动态内存大小为 = %u Bytes\r\n",xPortGetFreeHeapSize());
//		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
//		sprintf(buff,"%s \r\n","系统启动至当前时刻的动态内存最小剩余为 = %u Bytes\r\n",xPortGetMinimumEverFreeHeapSize());
//		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
//
//		vTaskList(CPU_RunInfo);  //获取任务信息
//	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" ,38, HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);
//
//	  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
//	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer2 , strlen(buffer2), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);

	if(xTimerStop(MyTimer01Handle,100) == pdPASS)
	{
		sprintf(buff,"%s \r\n","关闭定时器1成功");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	}
	else
	{
		sprintf(buff,"%s \r\n","关闭定时器1失败");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	}

	if(xTimerStop(MyTimer02Handle,100) == pdPASS)
	{
		sprintf(buff,"%s \r\n","关闭定时器2成功");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	}
	else
	{
		sprintf(buff,"%s \r\n","关闭定时器2失败");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	}
	}

	//KEY2处理
	if(KEY.KeyCode==KEY2){
		if(xTimerStart(MyTimer01Handle,100) == pdPASS)
		{
			sprintf(buff,"%s \r\n","重新打开定时器1成功");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
		else
		{
			sprintf(buff,"%s \r\n","重新打开定时器1失败");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}

		if(xTimerStart(MyTimer02Handle,100) == pdPASS)
		{
			sprintf(buff,"%s \r\n","重新打开定时器2成功");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
		else
		{
			sprintf(buff,"%s \r\n","重新打开定时器2失败");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
	}
	osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_LED_G_Task */
/**
* @brief Function implementing the LED_G thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_G_Task */
void LED_G_Task(void const * argument)
{
  /* USER CODE BEGIN LED_G_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END LED_G_Task */
}

/* USER CODE BEGIN Header_LED_R_Task */
/**
* @brief Function implementing the LED_R thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_R_Task */
void LED_R_Task(void const * argument)
{
  /* USER CODE BEGIN LED_R_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
    HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
  }
  /* USER CODE END LED_R_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(huart->Instance == huart2.Instance)
	{

		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}
/* USER CODE END Application */

