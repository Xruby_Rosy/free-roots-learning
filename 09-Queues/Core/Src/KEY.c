/* Includes ------------------------------------------------------------------*/
#include "KEY.h"

/* Private define-------------------------------------------------------------*/

/* Private variables----------------------------------------------------------*/
static void GetKeyCode(void);

/* Public variables-----------------------------------------------------------*/
KEY_t KEY = 
{
	KEYNULL,
	
	GetKeyCode
};

/* Private function prototypes------------------------------------------------*/      

/*
	* @name   GetKeyCode
	* @brief  获取按键编码
	* @param  None
	* @retval None      
*/
static void GetKeyCode(void)
{
	KeyCode_t KeyCode = KEYNULL;
	
	//检测KEY1
	if(KeyCode == KEYNULL)
	{
		if(HAL_GPIO_ReadPin(KEY1_GPIO_Port,KEY1_Pin) == GPIO_PIN_RESET)
		{
			KeyCode = KEY1;
			//等待按键KEY1释放
			while(HAL_GPIO_ReadPin(KEY1_GPIO_Port,KEY1_Pin) == GPIO_PIN_RESET)
			{
				osDelay(20); //必须用阻塞延时 此时是用操作系统延时，如果用HAL_Delay延时是空操作，如果任务优先级比较高，则其他任务就不会延时
			}
		}
	}
	//检测KEY2
	if(KeyCode == KEYNULL)
	{
		if(HAL_GPIO_ReadPin(KEY2_GPIO_Port,KEY2_Pin) == GPIO_PIN_RESET)
		{
			KeyCode = KEY2;
			//等待KEY2释放
			while(HAL_GPIO_ReadPin(KEY2_GPIO_Port,KEY2_Pin) == GPIO_PIN_RESET)
			{
				osDelay(20);  //必须用阻塞延时
			}
		}
	}
	//更新按键编码值ֵ
	KEY.KeyCode = KeyCode;
}
/********************************************************
  End Of File
********************************************************/
