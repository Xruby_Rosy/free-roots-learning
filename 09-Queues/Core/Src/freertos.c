/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"usart.h"
#include"stdio.h"
#include"KEY.h"
#include"string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;

//定义队列变量
static QueueHandle_t xQueue1=NULL;
static QueueHandle_t xQueue2=NULL;
/* USER CODE END Variables */
osThreadId CPU_TaskHandle;
osThreadId LED_RHandle;
osThreadId LED_GHandle;
osThreadId LED_BHandle;
osThreadId KEYHandle;
osThreadId Queuel1Handle;
osThreadId Queuel2Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void Print_CPU_Task(void const * argument);
void LED_RED_Task02(void const * argument);
void LED_GREEN_Task03(void const * argument);
void LED_BLUE_Task04(void const * argument);
void KEY_Task(void const * argument);
void Queuel1_Task06(void const * argument);
void Queuel2_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
	xQueue1 = xQueueCreate(1, sizeof(uint32_t));
	if(xQueue1 == NULL)
		HAL_UART_Transmit(&huart2, (uint8_t*)"创建消息队列1失败\r\n", 13, HAL_MAX_DELAY);
	else
		HAL_UART_Transmit(&huart2, (uint8_t*)"创建消息队列1成功\r\n", 13, HAL_MAX_DELAY);

	xQueue2 = xQueueCreate(2, 16);
	if(xQueue2 == NULL)
		HAL_UART_Transmit(&huart2, (uint8_t*)"创建消息队列2失败\r\n", 13, HAL_MAX_DELAY);
	else
		HAL_UART_Transmit(&huart2, (uint8_t*)"创建消息队列2失败\r\n", 13, HAL_MAX_DELAY);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of CPU_Task */
  osThreadDef(CPU_Task, Print_CPU_Task, osPriorityAboveNormal, 0, 256);
  CPU_TaskHandle = osThreadCreate(osThread(CPU_Task), NULL);

  /* definition and creation of LED_R */
  osThreadDef(LED_R, LED_RED_Task02, osPriorityBelowNormal, 0, 128);
  LED_RHandle = osThreadCreate(osThread(LED_R), NULL);

  /* definition and creation of LED_G */
  osThreadDef(LED_G, LED_GREEN_Task03, osPriorityBelowNormal, 0, 128);
  LED_GHandle = osThreadCreate(osThread(LED_G), NULL);

  /* definition and creation of LED_B */
  osThreadDef(LED_B, LED_BLUE_Task04, osPriorityBelowNormal, 0, 128);
  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of Queuel1 */
  osThreadDef(Queuel1, Queuel1_Task06, osPriorityNormal, 0, 128);
  Queuel1Handle = osThreadCreate(osThread(Queuel1), NULL);

  /* definition and creation of Queuel2 */
  osThreadDef(Queuel2, Queuel2_Task, osPriorityNormal, 0, 128);
  Queuel2Handle = osThreadCreate(osThread(Queuel2), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_Print_CPU_Task */
/**
  * @brief  Function implementing the CPU_Task thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_Print_CPU_Task */
void Print_CPU_Task(void const * argument)
{
  /* USER CODE BEGIN Print_CPU_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END Print_CPU_Task */
}

/* USER CODE BEGIN Header_LED_RED_Task02 */
/**
* @brief Function implementing the LED_R thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_RED_Task02 */
void LED_RED_Task02(void const * argument)
{
  /* USER CODE BEGIN LED_RED_Task02 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(200);
    HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
  }
  /* USER CODE END LED_RED_Task02 */
}

/* USER CODE BEGIN Header_LED_GREEN_Task03 */
/**
* @brief Function implementing the LED_G thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_GREEN_Task03 */
void LED_GREEN_Task03(void const * argument)
{
  /* USER CODE BEGIN LED_GREEN_Task03 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(500);
    HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
  }
  /* USER CODE END LED_GREEN_Task03 */
}

/* USER CODE BEGIN Header_LED_BLUE_Task04 */
/**
* @brief Function implementing the LED_B thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_BLUE_Task04 */
void LED_BLUE_Task04(void const * argument)
{
  /* USER CODE BEGIN LED_BLUE_Task04 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1000);
    HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
  }
  /* USER CODE END LED_BLUE_Task04 */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
	char CPU_RunInfo[500];  //保存任务运行时间信息
	char ucSend_Data = 0;  //向消息队列1发送的数据
	const TickType_t ulSendBlockTime = pdMS_TO_TICKS(10);
	char buff[500];
	char buffer0[100]=" Name       State    Priority LeftStack     Number  \r\n";
	char buffer1[100]=" Name             RunCount             UtilizeRatio  \r\n";
  /* Infinite loop */
  for(;;)
  {
    //按键检测
	  KEY.GetKeyCode();
	  //KEY1处理
	  if(KEY.KeyCode==KEY1){
		  vTaskList(CPU_RunInfo);  //获取任务信息
		  HAL_UART_Transmit(&huart2,(uint8_t *)buffer0 , strlen(buffer0), HAL_MAX_DELAY);
		  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
		  HAL_UART_Transmit(&huart2, (uint8_t*)"------------------------------\r\n", 30, HAL_MAX_DELAY);

		  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
		  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
		  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
		  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

		  HAL_UART_Transmit(&huart2, (uint8_t*)"------------------------------ \r\n", 33, HAL_MAX_DELAY);
		  osDelay(2000);
	  }
	  if(KEY.KeyCode==KEY2){
		  if(xQueueSend(xQueue1,&ucSend_Data,ulSendBlockTime)==pdTRUE){//任务中消息队列发送，参数（消息队列句柄，要传递数据地址，等待消息队列有空间的最大等待时间）
			  sprintf(buff,"成功向消息队列1发送数据： %u\r\n",ucSend_Data);
		  }else{
			  HAL_UART_Transmit(&huart2, (uint8_t*)"向消息队列1发送数据出现超时：\r\n", 15, HAL_MAX_DELAY);

		  }
	  }
	  //阻塞延时20ms
		osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_Queuel1_Task06 */
/**
* @brief Function implementing the Queuel1 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Queuel1_Task06 */
void Queuel1_Task06(void const * argument)
{
  /* USER CODE BEGIN Queuel1_Task06 */
 char ucRec_Data = 0;
 char buff[100];
 const TickType_t ulReceiveBlockTime = pdMS_TO_TICKS(1000);
  /* Infinite loop */
  for(;;)
  {
	  if(xQueueReceive(xQueue1,&ucRec_Data,ulReceiveBlockTime)==pdTRUE){
		  sprintf(buff,"成功向消息队列1发送数据： %u\r\n",ucRec_Data);
		  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
	  }else{
		  HAL_UART_Transmit(&huart2, (uint8_t*)"向消息队列1发送数据出现超时：\r\n", 15, HAL_MAX_DELAY);

	  }
  }
  /* USER CODE END Queuel1_Task06 */
}

/* USER CODE BEGIN Header_Queuel2_Task */
/**
* @brief Function implementing the Queuel2 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Queuel2_Task */
void Queuel2_Task(void const * argument)
{
  /* USER CODE BEGIN Queuel2_Task */
	char ucRec_Data[16] = {0};
	char buff[100];
  /* Infinite loop */
  for(;;)
  {
	  if(xQueueReceive(xQueue2,&ucRec_Data,portMAX_DELAY)==pdTRUE){
		  sprintf(buff,"成功向消息队列2发送数据： %u\r\n",ucRec_Data);
		  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
	  }
	  osDelay(20);
  }
  /* USER CODE END Queuel2_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

