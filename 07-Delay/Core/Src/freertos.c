/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"usart.h"
#include<stdio.h>
#include<string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId CPU_TaskHandle;
osThreadId LED_B_Task02Handle;
osThreadId LED_G_Task03Handle;
osThreadId LED_R_Task04Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void CPU_Task01(void const * argument);
void LED_BLUE02(void const * argument);
void LED_GREEN03(void const * argument);
void LED_RED04(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of CPU_Task */
  osThreadDef(CPU_Task, CPU_Task01, osPriorityAboveNormal, 0, 256);
  CPU_TaskHandle = osThreadCreate(osThread(CPU_Task), NULL);

  /* definition and creation of LED_B_Task02 */
  osThreadDef(LED_B_Task02, LED_BLUE02, osPriorityBelowNormal, 0, 128);
  LED_B_Task02Handle = osThreadCreate(osThread(LED_B_Task02), NULL);

  /* definition and creation of LED_G_Task03 */
  osThreadDef(LED_G_Task03, LED_GREEN03, osPriorityNormal, 0, 128);
  LED_G_Task03Handle = osThreadCreate(osThread(LED_G_Task03), NULL);

  /* definition and creation of LED_R_Task04 */
  osThreadDef(LED_R_Task04, LED_RED04, osPriorityBelowNormal, 0, 128);
  LED_R_Task04Handle = osThreadCreate(osThread(LED_R_Task04), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_CPU_Task01 */
/**
  * @brief  Function implementing the CPU_Task thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_CPU_Task01 */
void CPU_Task01(void const * argument)
{
  /* USER CODE BEGIN CPU_Task01 */
	uint16_t Task1_Cnt=0;
	char buff[100];
  /* Infinite loop */
  for(;;)
  {
//	HAL_UART_Transmit(&huart2, (uint8_t *)"------------Delay Start!---------------\r\n", 39, HAL_MAX_DELAY);
//    osDelay(1000);
  //模拟传感器采集数据与被中断或高优先级任务打断的时间
	  HAL_Delay(50);
	  HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
	  //打印任务运行次数
	  sprintf(buff,"任务一次执行次数：  %u \r\n",++Task1_Cnt);
	  HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
	  //绝对延时200ms
	  osDelay(200);
  }
  /* USER CODE END CPU_Task01 */
}

/* USER CODE BEGIN Header_LED_BLUE02 */
/**
* @brief Function implementing the LED_B_Task02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_BLUE02 */
void LED_BLUE02(void const * argument)
{
  /* USER CODE BEGIN LED_BLUE02 */
//	uint16_t Task1_Cnt=0;
//	char buff[100];
  /* Infinite loop */
  for(;;)
  {
//	  //模拟传感器采集数据与被中断或高优先级任务打断的时间
//	  HAL_Delay(50);
//	  HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
//	  //打印任务运行次数
//	  sprintf(buff,"任务一次执行次数： %u \r\n",++Task1_Cnt);
//	  //绝对延时200ms
    osDelay(200);
  }
  /* USER CODE END LED_BLUE02 */
}

/* USER CODE BEGIN Header_LED_GREEN03 */
/**
* @brief Function implementing the LED_G_Task03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_GREEN03 */
void LED_GREEN03(void const * argument)
{
  /* USER CODE BEGIN LED_GREEN03 */
	portTickType PreviousWakeTime;
	uint16_t Task2_Cnt = 0;
	//获取当前系统时间
	PreviousWakeTime = xTaskGetTickCount();
	char buff[50];
  /* Infinite loop */
  for(;;)
  {
	  //模拟传感器采集数据与被中断或高优先级任务打断的时间
	  HAL_Delay(50);
	  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
	  sprintf(buff,"任务二次执行次数：  %u\r\n",++Task2_Cnt);
	  HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
	  //绝对延时200ms
	  osDelayUntil(&PreviousWakeTime, 200);
  }
  /* USER CODE END LED_GREEN03 */
}

/* USER CODE BEGIN Header_LED_RED04 */
/**
* @brief Function implementing the LED_R_Task04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_RED04 */
void LED_RED04(void const * argument)
{
  /* USER CODE BEGIN LED_RED04 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END LED_RED04 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

