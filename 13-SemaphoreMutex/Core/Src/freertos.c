/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"KEY.h"
#include<stdio.h>
#include<string.h>
#include"usart.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;
uint8_t myCountingSem_ucMessagesWaiting = 20;
/* USER CODE END Variables */
osThreadId LEDHandle;
osThreadId KEYHandle;
osThreadId HighPriorityHandle;
osThreadId MidPriorityHandle;
osThreadId lowPriorityHandle;
osMutexId myMutex01Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void LED_Task(void const * argument);
void KEY_Task(void const * argument);
void HighPriority_Task(void const * argument);
void MidPriority_Task(void const * argument);
void lowPriority_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* definition and creation of myMutex01 */
  osMutexDef(myMutex01);
  myMutex01Handle = osMutexCreate(osMutex(myMutex01));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  if(myMutex01Handle==NULL){
	  HAL_UART_Transmit(&huart2, (uint8_t*)"创建互斥信号失败\r\n", 12, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,  (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
  }else{
	  HAL_UART_Transmit(&huart2, (uint8_t*)"创建互斥信号成功\r\n", 12, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,  (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
  }
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of LED */
  osThreadDef(LED, LED_Task, osPriorityBelowNormal, 0, 128);
  LEDHandle = osThreadCreate(osThread(LED), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of HighPriority */
  osThreadDef(HighPriority, HighPriority_Task, osPriorityAboveNormal, 0, 128);
  HighPriorityHandle = osThreadCreate(osThread(HighPriority), NULL);

  /* definition and creation of MidPriority */
  osThreadDef(MidPriority, MidPriority_Task, osPriorityNormal, 0, 128);
  MidPriorityHandle = osThreadCreate(osThread(MidPriority), NULL);

  /* definition and creation of lowPriority */
  osThreadDef(lowPriority, lowPriority_Task, osPriorityBelowNormal, 0, 128);
  lowPriorityHandle = osThreadCreate(osThread(lowPriority), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_LED_Task */
/**
  * @brief  Function implementing the LED thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_LED_Task */
void LED_Task(void const * argument)
{
  /* USER CODE BEGIN LED_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
    HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
    osDelay(500);
    HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
  }
  /* USER CODE END LED_Task */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
	char CPU_RunInfo[500];//保存任务运行时间信息
	char buffer1[100]="任务名       任务状态       优先级   剩余栈    任务序号 \r\n  ";
	char buffer2[100]="任务名       运行计数       利用率\r\n";
  /* Infinite loop */
  for(;;)
  {
  //按键检测
	  KEY.GetKeyCode();
  //KEY1处理
  if(KEY.KeyCode==KEY1){
	  vTaskList(CPU_RunInfo);  //获取任务信息
	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" ,38, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);

	  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer2 , strlen(buffer2), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);
  	  }
	osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_HighPriority_Task */
/**
* @brief Function implementing the HighPriority thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_HighPriority_Task */
void HighPriority_Task(void const * argument)
{
  /* USER CODE BEGIN HighPriority_Task */
	BaseType_t xResult;
	char buff[100];
  /* Infinite loop */
  for(;;)
  {
	  sprintf(buff,"%s \r\n","HighPriority_Task 获取互斥信号量");
	  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
	  xResult=xSemaphoreTake(myMutex01Handle,portMAX_DELAY);

	  if(xResult==pdTRUE){
		  sprintf(buff,"%s \r\n","HighPriority_Task Running");
		  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
	  }
	  sprintf(buff,"%s \r\n","HighPriority_Task 释放互斥信号量");
	  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
	  xResult=xSemaphoreGive(myMutex01Handle);
	  osDelay(500);
  }
  /* USER CODE END HighPriority_Task */
}

/* USER CODE BEGIN Header_MidPriority_Task */
/**
* @brief Function implementing the MidPriority thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_MidPriority_Task */
void MidPriority_Task(void const * argument)
{
  /* USER CODE BEGIN MidPriority_Task */
	char buff[100];
  /* Infinite loop */
  for(;;)
  {
	  sprintf(buff,"%s \r\n","MidPriority_Task Running");
	  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
    osDelay(500);
  }
  /* USER CODE END MidPriority_Task */
}

/* USER CODE BEGIN Header_lowPriority_Task */
/**
* @brief Function implementing the lowPriority thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_lowPriority_Task */
void lowPriority_Task(void const * argument)
{
  /* USER CODE BEGIN lowPriority_Task */
	BaseType_t xResult;
	char buff[100];
  /* Infinite loop */
  for(;;)
  {
	  sprintf(buff,"%s \r\n","lowPriority_Task 获取互斥信号量");
	  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);

	  xResult=xSemaphoreTake(myMutex01Handle,portMAX_DELAY);

	  if(xResult==pdTRUE){
		  sprintf(buff,"%s \r\n","lowPriority_Task Running");
		  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);

	  }
	  HAL_Delay(3000);
	  sprintf(buff,"%s \r\n","lowPriority_Task 释放互斥信号量");
	  HAL_UART_Transmit(&huart2, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
	  xResult=xSemaphoreGive(myMutex01Handle);
	  osDelay(500);
  }
  /* USER CODE END lowPriority_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

