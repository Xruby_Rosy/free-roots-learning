/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"usart.h"
#include"KEY.h"
#include"stdio.h"
#include"string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;
/* USER CODE END Variables */
osThreadId CPU_TaskHandle;
osThreadId LED_B_Task02Handle;
osThreadId LED_G_Task03Handle;
osThreadId LED_R_Task04Handle;
osThreadId KEYHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void CPU_Task01(void const * argument);
void LED_BLUE02(void const * argument);
void LED_GREEN03(void const * argument);
void LED_RED04(void const * argument);
void KEY_Task05(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime =0;  //绯荤粺璋冨害鐨勬椂鍊欒繘琛屾竻闆�
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of CPU_Task */
  osThreadDef(CPU_Task, CPU_Task01, osPriorityAboveNormal, 0, 256);
  CPU_TaskHandle = osThreadCreate(osThread(CPU_Task), NULL);

  /* definition and creation of LED_B_Task02 */
  osThreadDef(LED_B_Task02, LED_BLUE02, osPriorityBelowNormal, 0, 128);
  LED_B_Task02Handle = osThreadCreate(osThread(LED_B_Task02), NULL);

  /* definition and creation of LED_G_Task03 */
  osThreadDef(LED_G_Task03, LED_GREEN03, osPriorityBelowNormal, 0, 128);
  LED_G_Task03Handle = osThreadCreate(osThread(LED_G_Task03), NULL);

  /* definition and creation of LED_R_Task04 */
  osThreadDef(LED_R_Task04, LED_RED04, osPriorityBelowNormal, 0, 128);
  LED_R_Task04Handle = osThreadCreate(osThread(LED_R_Task04), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task05, osPriorityHigh, 0, 128);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_CPU_Task01 */
/**
  * @brief  Function implementing the CPU_Task thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_CPU_Task01 */
void CPU_Task01(void const * argument)
{
  /* USER CODE BEGIN CPU_Task01 */
	char CPU_RunInfo[256];
	char buffer0[100]=" Name       State    Priority LeftStack     Number  \r\n";
	char buffer1[100]=" Name             RunCount             UtilizeRatio  \r\n";
  /* Infinite loop */
  for(;;)
  {

	  vTaskList(CPU_RunInfo);  //获取任务信息
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer0 , strlen(buffer0), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	  HAL_UART_Transmit(&huart2, (uint8_t*)"------------------------------ \r\n", 33, HAL_MAX_DELAY);
	  osDelay(1000);
  }
  /* USER CODE END CPU_Task01 */
}

/* USER CODE BEGIN Header_LED_BLUE02 */
/**
* @brief Function implementing the LED_BTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_BLUE02 */
void LED_BLUE02(void const * argument)
{
  /* USER CODE BEGIN LED_BLUE02 */
	//static uint16_t Task1_cnt=0;
  /* Infinite loop */
  for(;;)
  {
	  //HAL_Delay(50);
	  HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
	  HAL_UART_Transmit(&huart2, (uint8_t *)"TASKing 01!\r\n", 11, HAL_MAX_DELAY);
	  osDelay(1000);
  }
  /* USER CODE END LED_BLUE02 */
}

/* USER CODE BEGIN Header_LED_GREEN03 */
/**
* @brief Function implementing the LED_G_Task03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_GREEN03 */
void LED_GREEN03(void const * argument)
{
  /* USER CODE BEGIN LED_GREEN03 */
  /* Infinite loop */
  for(;;)
  {
//	  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
	  osDelay(500);
  }
  /* USER CODE END LED_GREEN03 */
}

/* USER CODE BEGIN Header_LED_RED04 */
/**
* @brief Function implementing the LED_R_Task04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_RED04 */
void LED_RED04(void const * argument)
{
  /* USER CODE BEGIN LED_RED04 */
  /* Infinite loop */
  for(;;)
  {
//	HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
	osDelay(100);
  }
  /* USER CODE END LED_RED04 */
}

/* USER CODE BEGIN Header_KEY_Task05 */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task05 */
void KEY_Task05(void const * argument)
{
  /* USER CODE BEGIN KEY_Task05 */
  char buff[100];
  /* Infinite loop */
  for(;;)
  {
  //按键检测
		KEY.GetKeyCode();

		//key1处理
		if(KEY.KeyCode == KEY1)
	{
			//测试任务代码临界段
			sprintf(buff,"更改前的KEY任务的优先级为：%u\r\n",(uint16_t)uxTaskPriorityGet(NULL));
			//HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
			vTaskPrioritySet(NULL,2);
			sprintf(buff,"更改后的KEY任务的优先级为：%u\r\n",(uint16_t)uxTaskPriorityGet(NULL));
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
			
			//HAL_UART_Transmit(&huart2, (uint8_t*)"进入代码临界段\r\n",11, HAL_MAX_DELAY);
			taskENTER_CRITICAL();  //进入代码临界段

			HAL_UART_Transmit(&huart2, (uint8_t*)"5s\r\n",2, HAL_MAX_DELAY);
			HAL_Delay(5000);

			taskEXIT_CRITICAL();   //退出代码临界段
			HAL_UART_Transmit(&huart2, (uint8_t*)"退出代码临界段\r\n",11, HAL_MAX_DELAY);
			vTaskPrioritySet(NULL,5);
		}

		//KEY2检测
		if(KEY.KeyCode == KEY2)
		{

		}


		//阻塞延时20ms
	  	  osDelay(20);
	}
  /* USER CODE END KEY_Task05 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

