/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"KEY.h"
#include"usart.h"
#include<stdio.h>
#include<string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;

//定义二值信号量
osSemaphoreId xBinarySem = NULL;
/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId LED_BHandle;
osThreadId LED_GHandle;
osThreadId LED_RHandle;
osThreadId KEYHandle;
osThreadId BinarySem_SynHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void LED_B_Task(void const * argument);
void LED_G_Task(void const * argument);
void LED_R_Task(void const * argument);
void KEY_Task(void const * argument);
void BinarySem_Syn_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
	xBinarySem = xSemaphoreCreateBinary();
	if(xBinarySem == NULL)
		HAL_UART_Transmit(&huart2, (uint8_t *)"创建二值信号量失败 FAIL\r\n\r\n", 22, HAL_MAX_DELAY);
	else
		HAL_UART_Transmit(&huart2, (uint8_t *)"创建二值信号量成功 Succeed\r\n\r\n", 25, HAL_MAX_DELAY);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of LED_B */
  osThreadDef(LED_B, LED_B_Task, osPriorityBelowNormal, 0, 128);
  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);

  /* definition and creation of LED_G */
  osThreadDef(LED_G, LED_G_Task, osPriorityBelowNormal, 0, 128);
  LED_GHandle = osThreadCreate(osThread(LED_G), NULL);

  /* definition and creation of LED_R */
  osThreadDef(LED_R, LED_R_Task, osPriorityBelowNormal, 0, 128);
  LED_RHandle = osThreadCreate(osThread(LED_R), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of BinarySem_Syn */
  osThreadDef(BinarySem_Syn, BinarySem_Syn_Task, osPriorityNormal, 0, 128);
  BinarySem_SynHandle = osThreadCreate(osThread(BinarySem_Syn), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_LED_B_Task */
/**
* @brief Function implementing the LED_B thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_B_Task */
void LED_B_Task(void const * argument)
{
  /* USER CODE BEGIN LED_B_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
    HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
  }
  /* USER CODE END LED_B_Task */
}

/* USER CODE BEGIN Header_LED_G_Task */
/**
* @brief Function implementing the LED_G thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_G_Task */
void LED_G_Task(void const * argument)
{
  /* USER CODE BEGIN LED_G_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(500);
   // HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
  }
  /* USER CODE END LED_G_Task */
}

/* USER CODE BEGIN Header_LED_R_Task */
/**
* @brief Function implementing the LED_R thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_R_Task */
void LED_R_Task(void const * argument)
{
  /* USER CODE BEGIN LED_R_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1000);
    HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
  }
  /* USER CODE END LED_R_Task */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
	char CPU_RunInfo[256];
	char buffer0[100]=" Name       State    Priority LeftStack     Number  \r\n";
	char buffer1[100]=" Name             RunCount             UtilizeRatio  \r\n";
	char buff[100];
	BaseType_t xResult;
	uint16_t GiveCnt = 0; //释放计数
	/* Infinite loop */
  for(;;)
  {
//按键检测
  KEY.GetKeyCode();
/************删除任务:通过按键1将正在执行的任务删除******************/
  //KEY1处理:判断任务是否产生
  if(KEY.KeyCode==KEY1){
  vTaskList(CPU_RunInfo);  //获取任务信息
  HAL_UART_Transmit(&huart2,(uint8_t *)buffer0 , strlen(buffer0), HAL_MAX_DELAY);
  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

  HAL_UART_Transmit(&huart2, (uint8_t*)"------------------------------ \r\n", 33, HAL_MAX_DELAY);
  }
  if(KEY.KeyCode == KEY2)
	{
	  HAL_UART_Transmit(&huart2, (uint8_t*)"Send Semaphore\r\n", 18, HAL_MAX_DELAY);
	  xResult = xSemaphoreGive(xBinarySem);

		if(xResult == pdTRUE)
		{
			sprintf(buff,"Succeed Send TWOSemaphore,GiveCnt= %u\r\n",++GiveCnt);
			HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
		}
		else
		{
			HAL_UART_Transmit(&huart2, (uint8_t*)"Fail\r\n", 6, HAL_MAX_DELAY);
		}
	}
	  osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_BinarySem_Syn_Task */
/**
* @brief Function implementing the BinarySem_Syn thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_BinarySem_Syn_Task */
void BinarySem_Syn_Task(void const * argument)
{
  /* USER CODE BEGIN BinarySem_Syn_Task */
	BaseType_t xResult;
	uint16_t TakeCnt = 0; //获取计数
	char buff[100];
  /* Infinite loop */
 for(;;)
  {
	 HAL_UART_Transmit(&huart2, (uint8_t*)"waiting\r\n", 4, HAL_MAX_DELAY);
	xResult = xSemaphoreTake(xBinarySem,portMAX_DELAY);

	if(xResult == pdTRUE)
	{
		sprintf(buff,"Succeed Receive TWOSemaphore,GiveCnt= %u\r\n",++TakeCnt);
		HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
		HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
	}
  }
  /* USER CODE END BinarySem_Syn_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

