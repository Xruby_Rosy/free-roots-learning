/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"usart.h"
#include"gpio.h"
#include"stdio.h"
#include"string.h"
#include"KEY.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;
/* USER CODE END Variables */
osThreadId CPU_TaskHandle;
osThreadId LED_RHandle;
osThreadId LED_GHandle;
osThreadId LED_BHandle;
osThreadId KEY_TaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void Print_CPU_Task(void const * argument);
void LED_RED_Task02(void const * argument);
void LED_GREEN_Task03(void const * argument);
void LED_BLUE_Task04(void const * argument);
void KEY_Task05(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime =0;  //系统调度的时候进行清零
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
	HAL_UART_Transmit(&huart2, (uint8_t *)"Task:%s Stack overflow occurs!!\r\n", 31, HAL_MAX_DELAY);
}
/* USER CODE END 4 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of CPU_Task */
  osThreadDef(CPU_Task, Print_CPU_Task, osPriorityAboveNormal, 0, 256);
  CPU_TaskHandle = osThreadCreate(osThread(CPU_Task), NULL);

  /* definition and creation of LED_R */
  osThreadDef(LED_R, LED_RED_Task02, osPriorityBelowNormal, 0, 128);
  LED_RHandle = osThreadCreate(osThread(LED_R), NULL);

  /* definition and creation of LED_G */
  osThreadDef(LED_G, LED_GREEN_Task03, osPriorityBelowNormal, 0, 128);
  LED_GHandle = osThreadCreate(osThread(LED_G), NULL);

  /* definition and creation of LED_B */
  osThreadDef(LED_B, LED_BLUE_Task04, osPriorityBelowNormal, 0, 128);
  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);

  /* definition and creation of KEY_Task */
  osThreadDef(KEY_Task, KEY_Task05, osPriorityNormal, 0, 128);
  KEY_TaskHandle = osThreadCreate(osThread(KEY_Task), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_Print_CPU_Task */
/**
  * @brief  Function implementing the CPU_Task thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_Print_CPU_Task */
void Print_CPU_Task(void const * argument)
{
  /* USER CODE BEGIN Print_CPU_Task */
	char CPU_RunInfo[256];
	char buffer0[100]=" Name       State    Priority LeftStack     Number  \r\n";
	char buffer1[100]=" Name             RunCount             UtilizeRatio  \r\n";
  /* Infinite loop */
  for(;;)
  {
	  vTaskList(CPU_RunInfo);  //获取任务信息
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer0 , strlen(buffer0), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	  HAL_UART_Transmit(&huart2, (uint8_t*)"------------------------------ \r\n", 33, HAL_MAX_DELAY);
	  osDelay(2000);
  }
  /* USER CODE END Print_CPU_Task */
}

/* USER CODE BEGIN Header_LED_RED_Task02 */
/**
* @brief Function implementing the LED_R thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_RED_Task02 */
void LED_RED_Task02(void const * argument)
{
  /* USER CODE BEGIN LED_RED_Task02 */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
	  osDelay(100);
  }
  /* USER CODE END LED_RED_Task02 */
}

/* USER CODE BEGIN Header_LED_GREEN_Task03 */
/**
* @brief Function implementing the LED_G thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_GREEN_Task03 */
void LED_GREEN_Task03(void const * argument)
{
  /* USER CODE BEGIN LED_GREEN_Task03 */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
	  osDelay(500);osDelay(1);
  }
  /* USER CODE END LED_GREEN_Task03 */
}

/* USER CODE BEGIN Header_LED_BLUE_Task04 */
/**
* @brief Function implementing the LED_B thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_BLUE_Task04 */
void LED_BLUE_Task04(void const * argument)
{
  /* USER CODE BEGIN LED_BLUE_Task04 */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
	  osDelay(1000);
  }
  /* USER CODE END LED_BLUE_Task04 */
}

/* USER CODE BEGIN Header_KEY_Task05 */
/**
* @brief Function implementing the KEY_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task05 */
void KEY_Task05(void const * argument)
{
  /* USER CODE BEGIN KEY_Task05 */
  /* Infinite loop */
  for(;;)
  {
  //按键检测
	  KEY.GetKeyCode();
 /************删除任务:通过按键1将正在执行的任务删除******************/
	  //KEY1处理:判断任务是否产生
	  if(KEY.KeyCode==KEY1){
		  HAL_UART_Transmit(&huart2, (uint8_t *)"KEY1 Down!\r\n",12 , HAL_MAX_DELAY);
		  uint16_t i;
		  uint8_t Buf[1024];
		  for(i=1023;i>=0;i--){
			  Buf[i]=0x55;
			  vTaskDelay(1);
		  }

	  }
 /************创建任务：通过按键2进行任务重新创建******************/
	  //KEY2处理：判断任务是否产生
//	  if(KEY.KeyCode==KEY2){
//		  HAL_UART_Transmit(&huart2, (uint8_t *)"KEY2 Down!\r\n",10 , HAL_MAX_DELAY);
//		  //判断任务LED_B是否为空
//		  if(LED_BHandle == NULL){
//			  //不为空时,进行LED_BHandle创建
//			  osThreadDef(LED_B, LED_BLUE_Task04, osPriorityBelowNormal, 0, 128);
//			  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);
//			  //再次进行判断：判断判断任务LED_B是否为空
//			  if(LED_BHandle != NULL){
//				  //不为空时，创建成功
//				  HAL_UART_Transmit(&huart2, (uint8_t *)"Cecreating Succeed!\r\n",19 , HAL_MAX_DELAY);
//			  }else{
//				  HAL_UART_Transmit(&huart2, (uint8_t *)"KEY2 EXIST! NO Need to Create!\r\n",30 , HAL_MAX_DELAY);
//			  }
//		  }
//	  }
 /************挂起任务：通过按键1将正在运行的任务进行挂起不断开******************/
//	  if(KEY.KeyCode==KEY1){
//		  HAL_UART_Transmit(&huart2, (uint8_t *)"KEY1 DOWN! Suspended Task LED_G\r\n",33 , HAL_MAX_DELAY);
//		  vTaskSuspend(LED_GHandle);
//	  }
 /************恢复任务：通过按键2将正在挂起的任务恢复到原始状态******************/
//	  if(KEY.KeyCode==KEY2){
//		  HAL_UART_Transmit(&huart2, (uint8_t *)"KEY2 DOWN! Recovery Task LED_G\r\n",32, HAL_MAX_DELAY);
//		  vTaskResume(LED_GHandle);
//	  }
	 osDelay(20);//阻塞延时20ms
  }
  /* USER CODE END KEY_Task05 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

