/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"KEY.h"
#include<stdio.h>
#include<string.h>
#include"usart.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
extern volatile uint32_t CPU_RunTime;
#define KEY1_EVENT  (EventBits_t)(0x0001 << 0)//设置事件掩码位0
#define KEY2_EVENT  (EventBits_t)(0x0001 << 8)//设置事件掩码位8
#define UART2_RecEVENT	(EventBits_t)(0x0001 << 23) //设置事件掩码位23
#define Event_WaitAllBits //预编译，看是否需要等待所有事件
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

char buff[100];
EventGroupHandle_t MyEvent01Handle=NULL; //不能配置，只能自己写，进行事件定义
/* USER CODE END Variables */
osThreadId LED_BHandle;
osThreadId KEYHandle;
osThreadId LED_GHandle;
osThreadId LED_RHandle;
osThreadId Event_SyncHandle;
osThreadId Event_SyncISRHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void LED_B_Task(void const * argument);
void KEY_Task(void const * argument);
void LED_G_Task(void const * argument);
void LED_R_Task(void const * argument);
void Event_Sync_Task(void const * argument);
void Event_SyncISR_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
 MyEvent01Handle = xEventGroupCreate();
  if(MyEvent01Handle==NULL)
  {
		sprintf(buff,"%s \r\n","创建事件组失败");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
  }
  else
  {
		sprintf(buff,"%s \r\n","创建事件组成功");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
  }
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of LED_B */
  osThreadDef(LED_B, LED_B_Task, osPriorityBelowNormal, 0, 128);
  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of LED_G */
  osThreadDef(LED_G, LED_G_Task, osPriorityBelowNormal, 0, 128);
  LED_GHandle = osThreadCreate(osThread(LED_G), NULL);

  /* definition and creation of LED_R */
  osThreadDef(LED_R, LED_R_Task, osPriorityBelowNormal, 0, 128);
  LED_RHandle = osThreadCreate(osThread(LED_R), NULL);

  /* definition and creation of Event_Sync */
  osThreadDef(Event_Sync, Event_Sync_Task, osPriorityNormal, 0, 128);
  Event_SyncHandle = osThreadCreate(osThread(Event_Sync), NULL);

  /* definition and creation of Event_SyncISR */
  osThreadDef(Event_SyncISR, Event_SyncISR_Task, osPriorityNormal, 0, 128);
  Event_SyncISRHandle = osThreadCreate(osThread(Event_SyncISR), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_LED_B_Task */
/**
  * @brief  Function implementing the LED_B thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_LED_B_Task */
void LED_B_Task(void const * argument)
{
  /* USER CODE BEGIN LED_B_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
    HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
  }
  /* USER CODE END LED_B_Task */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
	char CPU_RunInfo[500];//保存任务运行时间信息
	char buffer1[100]="任务名       任务状态       优先级   剩余栈    任务序号 \r\n  ";
		char buffer2[100]="任务名       运行计数       利用率\r\n";
  /* Infinite loop */
  for(;;)
  {
  //按键检测
	  KEY.GetKeyCode();
	//KEY1处理
	if(KEY.KeyCode==KEY1){
	  vTaskList(CPU_RunInfo);  //获取任务信息
	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" ,38, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);

	  vTaskGetRunTimeStats(CPU_RunInfo); //统计CPU占用时间，资源等
	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer2 , strlen(buffer2), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);

	  sprintf(buff,"%s \r\n","触发 KEY1 事件");
	  HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	  xEventGroupSetBits(MyEvent01Handle, KEY1_EVENT);
	}
	//KEY2
	  if(KEY.KeyCode==KEY2)
	  {
		sprintf(buff,"%s \r\n","触发 KEY2 事件");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		xEventGroupSetBits(MyEvent01Handle, KEY2_EVENT);
	  }
	osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_LED_G_Task */
/**
* @brief Function implementing the LED_G thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_G_Task */
void LED_G_Task(void const * argument)
{
  /* USER CODE BEGIN LED_G_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(500);
    HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
  }
  /* USER CODE END LED_G_Task */
}

/* USER CODE BEGIN Header_LED_R_Task */
/**
* @brief Function implementing the LED_R thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_R_Task */
void LED_R_Task(void const * argument)
{
  /* USER CODE BEGIN LED_R_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1000);
    HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
  }
  /* USER CODE END LED_R_Task */
}

/* USER CODE BEGIN Header_Event_Sync_Task */
/**
* @brief Function implementing the Event_Sync thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Event_Sync_Task */
void Event_Sync_Task(void const * argument)
{
  /* USER CODE BEGIN Event_Sync_Task */
	EventBits_t xEvent;
	int SyncCnt=0; //同锟斤拷锟斤拷锟斤拷
  /* Infinite loop */
#ifdef Event_WaitAllBits
	for(;;)
	{
		sprintf(buff,"%s \r\n","等待事件同步信号，无限等待");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		xEvent = xEventGroupWaitBits(
				MyEvent01Handle, //事件句柄
				KEY1_EVENT|KEY2_EVENT, //事件 按键1,2
				pdTRUE,          //退出时清除事件位
				pdTRUE,         //"逻辑与" - 满足任一事件
				portMAX_DELAY    //无限等待
		);
		if( (xEvent&(KEY1_EVENT|KEY2_EVENT)) == (KEY1_EVENT|KEY2_EVENT))
		{
			sprintf(buff,"成功接受到事件同步信号,次数  = %u\r\n",++SyncCnt);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
	}
#else
	for(;;)
	{
		sprintf(buff,"%s \r\n","等待事件同步信号，无限等待");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		xEvent = xEventGroupWaitBits(
				MyEvent01Handle, //事件句柄
				KEY1_EVENT|KEY2_EVENT, //事件 按键1,2
				pdTRUE,          //退出时清除事件位
				pdFALSE,         //"逻辑或" - 满足任一事件
				portMAX_DELAY    //无限等待
		);
		if(((xEvent&KEY1_EVENT) == KEY1_EVENT) || ((xEvent&KEY2_EVENT) == KEY2_EVENT))
		{
			sprintf(buff,"成功接受到事件同步信号,次数 = %d\r\n",++SyncCnt);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
	}
#endif
  /* USER CODE END Event_Sync_Task */
}

/* USER CODE BEGIN Header_Event_SyncISR_Task */
/**
* @brief Function implementing the Event_SyncISR thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Event_SyncISR_Task */
void Event_SyncISR_Task(void const * argument)
{
  /* USER CODE BEGIN Event_SyncISR_Task */
	EventBits_t xEvent;
	char rxBuff[13];
  /* Infinite loop */
	for(;;)
	{
		//通过串口2中断接收12个字符
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rxBuff, 12);

		sprintf(buff,"请通过UART3发送12个字符\r\n");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);

		sprintf(buff,"等待事件同步信号，无限等待\r\n");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);

		xEvent = xEventGroupWaitBits(
				MyEvent01Handle, //事件句柄
				UART2_RecEVENT,  //事件 - 串口3接收
				pdTRUE,          //退出时清除事件位
				pdTRUE,          //"逻辑与" - 满足所有事件
				portMAX_DELAY    //无限等待
		);
		if((xEvent&UART2_RecEVENT) == UART2_RecEVENT)
		{
			sprintf(buff,"接收到的串口数据：%s\r\n\r\n",rxBuff);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rxBuff, 12);
	}
  /* USER CODE END Event_SyncISR_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

