/* Includes ------------------------------------------------------------------*/
#include "MyApplication.h"

/* Private define-------------------------------------------------------------*/

/* Private variables----------------------------------------------------------*/
static void Peripheral_Set(void); 

/* Public variables-----------------------------------------------------------*/
MyInit_t MyInit = 
{
	Peripheral_Set
};

/* Private function prototypes------------------------------------------------*/      


/*
	* @name   Peripheral_Set
	* @brief  外设设置
	* @param  None
	* @retval None      
*/
static void Peripheral_Set()
{
	printf("----FreeRTOS操作系统----\r\n");
	printf("串口打印任务情况\r\n\r\n");

  HAL_TIM_Base_Start_IT(&htim3); //启动定时器3
}

/********************************************************
  End Of File
********************************************************/
