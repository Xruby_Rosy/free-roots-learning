/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"KEY.h"
#include"usart.h"
#include<stdio.h>
#include<string.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
//定义结构体
typedef struct
{
	uint8_t FeedDog_Flag;  //喂狗标志

	void (*FeedDog)(void); //喂狗
} MyIWDG_t;

typedef enum
{
  FALSE = 0U,
  TRUE = !FALSE
} FlagStatus_t;  //定义False与true标志位

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BIT_Task01_EVENT	(EventBits_t)(0x0001 << 0) //设置时间掩码位0
#define BIT_Task02_EVENT	(EventBits_t)(0x0001 << 1) //设置时间掩码位1
#define BIT_Task03_EVENT	(EventBits_t)(0x0001 << 2) //设置时间掩码位2
#define BIT_Task04_EVENT	(EventBits_t)(0x0001 << 3) //设置时间掩码位3
#define BIT_TaskAll_EVENT BIT_Task01_EVENT | BIT_Task02_EVENT | BIT_Task03_EVENT | BIT_Task04_EVENT
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
static void FeedDog(void);
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;
extern  MyIWDG_t  MyIWDG;  //定义看门狗
//定义事件组
EventGroupHandle_t MyEvent01Handle = NULL;
IWDG_HandleTypeDef hiwdg;
char buff[100];

/* USER CODE END Variables */
osThreadId LED_BHandle;
osThreadId KEYHandle;
osThreadId LED_GHandle;
osThreadId LED_RHandle;
osThreadId IWDG_MonitorHandle;
osThreadId Task01Handle;
osThreadId Task02Handle;
osThreadId Task03Handle;
osThreadId Task04Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
//看门狗实现
MyIWDG_t MyIWDG =
{
	TRUE,

	FeedDog
};
static void FeedDog(void)
{
	HAL_IWDG_Refresh(&hiwdg);  //调用看门狗进行刷新
}
/* USER CODE END FunctionPrototypes */

void LED_B_Task(void const * argument);
void KEY_Task(void const * argument);
void LED_G_Task(void const * argument);
void LED_R_Task(void const * argument);
void IWDG_Monito_Task(void const * argument);
void Task01_Entry(void const * argument);
void Task02_Entry(void const * argument);
void Task03_Entry(void const * argument);
void Task04_Entry(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
//创建事件组
	MyEvent01Handle=xEventGroupCreate();
	if(MyEvent01Handle==NULL){
		sprintf(buff,"%s \r\n","创建事件组失败");
		HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);

	}else{
		sprintf(buff,"%s \r\n","创建事件组成功");
		HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);

	}
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of LED_B */
  osThreadDef(LED_B, LED_B_Task, osPriorityBelowNormal, 0, 128);
  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of LED_G */
  osThreadDef(LED_G, LED_G_Task, osPriorityBelowNormal, 0, 128);
  LED_GHandle = osThreadCreate(osThread(LED_G), NULL);

  /* definition and creation of LED_R */
  osThreadDef(LED_R, LED_R_Task, osPriorityBelowNormal, 0, 128);
  LED_RHandle = osThreadCreate(osThread(LED_R), NULL);

  /* definition and creation of IWDG_Monitor */
  osThreadDef(IWDG_Monitor, IWDG_Monito_Task, osPriorityRealtime, 0, 128);
  IWDG_MonitorHandle = osThreadCreate(osThread(IWDG_Monitor), NULL);

  /* definition and creation of Task01 */
  osThreadDef(Task01, Task01_Entry, osPriorityNormal, 0, 128);
  Task01Handle = osThreadCreate(osThread(Task01), NULL);

  /* definition and creation of Task02 */
  osThreadDef(Task02, Task02_Entry, osPriorityAboveNormal, 0, 128);
  Task02Handle = osThreadCreate(osThread(Task02), NULL);

  /* definition and creation of Task03 */
  osThreadDef(Task03, Task03_Entry, osPriorityHigh, 0, 128);
  Task03Handle = osThreadCreate(osThread(Task03), NULL);

  /* definition and creation of Task04 */
  osThreadDef(Task04, Task04_Entry, osPriorityBelowNormal, 0, 128);
  Task04Handle = osThreadCreate(osThread(Task04), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_LED_B_Task */
/**
  * @brief  Function implementing the LED_B thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_LED_B_Task */
void LED_B_Task(void const * argument)
{
  /* USER CODE BEGIN LED_B_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
    HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
  }
  /* USER CODE END LED_B_Task */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
  /* Infinite loop */
  for(;;)
  {
	  //按键检测
	  KEY.GetKeyCode();
	  //KEY1检测
	  if(KEY.KeyCode==KEY1)
	  {
		  //挂起task1
		  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
		  sprintf(buff,"%s \r\n","挂起task1的任务");
		  HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
		  vTaskSuspend(Task01Handle);
	  }
  //KEY2检测
//	  if(KEY.KeyCode==KEY2)
//	  {
//		  //删除task1
//		  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
//		  sprintf(buff,"%s \r\n","删除task1的任务");
//		  HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
//		  vTaskSuspend(Task01Handle);
//	  }
	  //KEY2检测
		  if(KEY.KeyCode==KEY2)
		  {
			  //挂起IWDG_Monitor任务
			  HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
			  sprintf(buff,"%s \r\n","挂起IWDG_Monitor任务");
			  HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
			  vTaskSuspend(IWDG_MonitorHandle);
		  }
    osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_LED_G_Task */
/**
* @brief Function implementing the LED_G thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_G_Task */
void LED_G_Task(void const * argument)
{
  /* USER CODE BEGIN LED_G_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
    //HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
  }
  /* USER CODE END LED_G_Task */
}

/* USER CODE BEGIN Header_LED_R_Task */
/**
* @brief Function implementing the LED_R thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_R_Task */
void LED_R_Task(void const * argument)
{
  /* USER CODE BEGIN LED_R_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
    //HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
  }
  /* USER CODE END LED_R_Task */
}

/* USER CODE BEGIN Header_IWDG_Monito_Task */
/**
* @brief Function implementing the IWDG_Monitor thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_IWDG_Monito_Task */
void IWDG_Monito_Task(void const * argument)
{
  /* USER CODE BEGIN IWDG_Monito_Task */
	EventBits_t xEvent;
	const TickType_t xTicksToWait= 6000/portTICK_PERIOD_MS;//设置等待时间为6s

  /* Infinite loop */
  for(;;)
  {
	  xEvent = xEventGroupWaitBits(
			  MyEvent01Handle,    //事件句柄
			  BIT_TaskAll_EVENT,  //事件
			  pdTRUE,             //退出时清除事件位
			  pdTRUE,             //"逻辑与"--满足所有事件
			  xTicksToWait        //等待时间
			  );
	  if((xEvent&(BIT_TaskAll_EVENT))==(BIT_TaskAll_EVENT)){
		  HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
		  sprintf(buff,"%s \r\n","喂狗，监测任务与被监测任务均正常运行\r\n");
		  HAL_UART_Transmit(&huart2, (uint8_t *)buff, strlen(buff), HAL_MAX_DELAY);
		  MyIWDG.FeedDog();
	  }else{

	  }
    osDelay(20);
  }
  /* USER CODE END IWDG_Monito_Task */
}

/* USER CODE BEGIN Header_Task01_Entry */
/**
* @brief Function implementing the Task01 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task01_Entry */
void Task01_Entry(void const * argument)
{
  /* USER CODE BEGIN Task01_Entry */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1000);
    xEventGroupSetBits(MyEvent01Handle, BIT_Task01_EVENT);
  }
  /* USER CODE END Task01_Entry */
}

/* USER CODE BEGIN Header_Task02_Entry */
/**
* @brief Function implementing the Task02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task02_Entry */
void Task02_Entry(void const * argument)
{
  /* USER CODE BEGIN Task02_Entry */
  /* Infinite loop */
  for(;;)
  {
    osDelay(2000);
    xEventGroupSetBits(MyEvent01Handle, BIT_Task02_EVENT);
  }
  /* USER CODE END Task02_Entry */
}

/* USER CODE BEGIN Header_Task03_Entry */
/**
* @brief Function implementing the Task03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task03_Entry */
void Task03_Entry(void const * argument)
{
  /* USER CODE BEGIN Task03_Entry */
  /* Infinite loop */
  for(;;)
  {
    osDelay(3000);
    xEventGroupSetBits(MyEvent01Handle, BIT_Task03_EVENT);
  }
  /* USER CODE END Task03_Entry */
}

/* USER CODE BEGIN Header_Task04_Entry */
/**
* @brief Function implementing the Task04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task04_Entry */
void Task04_Entry(void const * argument)
{
  /* USER CODE BEGIN Task04_Entry */
  /* Infinite loop */
  for(;;)
  {
    osDelay(4000);
    xEventGroupSetBits(MyEvent01Handle, BIT_Task04_EVENT);
  }
  /* USER CODE END Task04_Entry */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

