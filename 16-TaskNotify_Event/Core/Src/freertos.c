/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"KEY.h"
#include<stdio.h>
#include<string.h>
#include"usart.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
extern volatile uint32_t CPU_RunTime;
#define KEY1_EVENT  (EventBits_t)(0x0001 << 0)//设置事件掩码位0
#define KEY2_EVENT  (EventBits_t)(0x0001 << 8)//设置事件掩码位8
//#define Event_WaitAllBits //预编译，看是否需要等待所有事件
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
char buff[100];
EventGroupHandle_t MyEvent01Handle=NULL;  //不能配置，只能自己写，进行事件定义
/* USER CODE END Variables */
osThreadId LEDHandle;
osThreadId KEYHandle;
osThreadId Event_SyncHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void LED_Task(void const * argument);
void KEY_Task(void const * argument);
void Event_Sync_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	MyEvent01Handle = xEventGroupCreate();
	  if(MyEvent01Handle==NULL)
	  {
			sprintf(buff,"%s \r\n","创建事件组失败");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	  }
	  else
	  {
			sprintf(buff,"%s \r\n","创建事件组成功�");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	  }
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of LED */
  osThreadDef(LED, LED_Task, osPriorityBelowNormal, 0, 128);
  LEDHandle = osThreadCreate(osThread(LED), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of Event_Sync */
  osThreadDef(Event_Sync, Event_Sync_Task, osPriorityNormal, 0, 128);
  Event_SyncHandle = osThreadCreate(osThread(Event_Sync), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_LED_Task */
/**
  * @brief  Function implementing the LED thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_LED_Task */
void LED_Task(void const * argument)
{
  /* USER CODE BEGIN LED_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
    HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
  }
  /* USER CODE END LED_Task */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
//	char CPU_RunInfo[500];//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷时锟斤拷锟斤拷息
//	char buffer1[100]="锟斤拷锟斤拷锟斤拷       锟斤拷锟斤拷状态       锟斤拷锟饺硷拷   剩锟斤拷栈    锟斤拷锟斤拷锟斤拷锟� \r\n  ";
//	char buffer2[100]="锟斤拷锟斤拷锟斤拷       锟斤拷锟叫硷拷锟斤拷       锟斤拷锟斤拷锟斤拷\r\n";
  /* Infinite loop */
  for(;;)
  {
  //锟斤拷锟斤拷锟斤拷锟�
	  KEY.GetKeyCode();
	//KEY1锟斤拷锟斤拷
	if(KEY.KeyCode==KEY1){
//	  vTaskList(CPU_RunInfo);  //锟斤拷取锟斤拷锟斤拷锟斤拷息
//	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" ,38, HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer1 , strlen(buffer1), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);
//
//	  vTaskGetRunTimeStats(CPU_RunInfo); //统锟斤拷CPU占锟斤拷时锟戒，锟斤拷源锟斤拷
//	  HAL_UART_Transmit(&huart2,(uint8_t *)buffer2 , strlen(buffer2), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)CPU_RunInfo , strlen(CPU_RunInfo), HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
//	  HAL_UART_Transmit(&huart2,(uint8_t *)"------------------------------------\r\n" , 38, HAL_MAX_DELAY);

	  sprintf(buff,"%s \r\n","发送任务通知 - 按键1事件");
	  HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
	  xTaskNotify(Event_SyncHandle,KEY1_EVENT,eSetBits);
	}
	//KEY2
	  if(KEY.KeyCode==KEY2)
	  {
		sprintf(buff,"%s \r\n","发送任务通知 - 按键2事件");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		xTaskNotify(Event_SyncHandle,KEY2_EVENT,eSetBits);
	  }
	osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_Event_Sync_Task */
/**
* @brief Function implementing the Event_Sync thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Event_Sync_Task */
void Event_Sync_Task(void const * argument)
{
  /* USER CODE BEGIN Event_Sync_Task */
	BaseType_t xResult;
	uint32_t Rec_Event = 0;  //定义任务通知接收变量
	uint32_t Last_Event = 0; //缓存任务通知之前的值，用于多个事件
	uint16_t SyncCnt = 0;    //同步计数
  /* Infinite loop */
#ifdef Event_WaitAllBits
	for(;;)
	{
		/*sprintf(buff,"%s \r\n","锟饺达拷锟铰硷拷同锟斤拷锟脚号ｏ拷锟斤拷锟睫等达拷");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		xEvent = xEventGroupWaitBits(
				MyEvent01Handle, //锟铰硷拷锟斤拷锟�
				KEY1_EVENT|KEY2_EVENT, //锟铰硷拷 锟斤拷锟斤拷1,2
				pdTRUE,          //锟剿筹拷时锟斤拷锟斤拷录锟轿�
				pdTRUE,         //"锟竭硷拷锟斤拷" - 锟斤拷锟斤拷锟斤拷一锟铰硷拷
				portMAX_DELAY    //锟斤拷锟睫等达拷
		);
		if( (xEvent&(KEY1_EVENT|KEY2_EVENT)) == (KEY1_EVENT|KEY2_EVENT))
		{
			sprintf(buff,"锟缴癸拷锟斤拷锟杰碉拷锟铰硷拷同锟斤拷锟脚猴拷,锟斤拷锟斤拷  = %u\r\n",++SyncCnt);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}*/
		sprintf(buff,"%s \r\n","获取任务通知，无限等待\r\n");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);

		xResult = xTaskNotifyWait(
				0x00000000,   //进入函数时不清除任务通知值任何位
				0xFFFFFFFF,   //退出函数时清除任务通知值所有位
				&Rec_Event,   //保存任务通知值
				portMAX_DELAY //阻塞时间
		 );
	if(xResult == pdTRUE)
	{
		Last_Event |= Rec_Event; //锟斤拷锟斤拷
		if((Last_Event&(KEY1_EVENT|KEY2_EVENT)) == (KEY1_EVENT|KEY2_EVENT)){
			sprintf(buff,"成功接受到任务通知同步信号(替代时间),次数 = %u\r\n",++SyncCnt);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
			HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
			Last_Event=0;
		}
	}
	}
#else
	for(;;)
	{
		/*sprintf(buff,"%s \r\n","锟饺达拷锟铰硷拷同锟斤拷锟脚号ｏ拷锟斤拷锟睫等达拷");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		xEvent = xEventGroupWaitBits(
				MyEvent01Handle, //锟铰硷拷锟斤拷锟�
				KEY1_EVENT|KEY2_EVENT, //锟铰硷拷 锟斤拷锟斤拷1,2
				pdTRUE,          //锟剿筹拷时锟斤拷锟斤拷录锟轿�
				pdFALSE,         //"锟竭硷拷锟斤拷" - 锟斤拷锟斤拷锟斤拷一锟铰硷拷
				portMAX_DELAY    //锟斤拷锟睫等达拷
		);
		if(((xEvent&KEY1_EVENT) == KEY1_EVENT) || ((xEvent&KEY2_EVENT) == KEY2_EVENT))
		{
			sprintf(buff,"锟缴癸拷锟斤拷锟杰碉拷锟铰硷拷同锟斤拷锟脚猴拷,锟斤拷锟斤拷 = %d\r\n",++SyncCnt);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}*/

		sprintf(buff,"%s \r\n","获取任务通知，无限等待\r\n");
				HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);

				xResult = xTaskNotifyWait(
						0x00000000,   //进入函数时不清除任务通知值任何位
						0xFFFFFFFF,   //退出函数时清除任务通知值所有位
						&Rec_Event,   //保存任务通知值
						portMAX_DELAY //阻塞时间
				 );
			if(xResult == pdTRUE)
			{
				Last_Event |= Rec_Event; //锟斤拷锟斤拷
				if(((Rec_Event&KEY1_EVENT) == KEY1_EVENT) || ((Rec_Event&KEY2_EVENT) == KEY2_EVENT)){
					sprintf(buff,"成功接受到任务通知同步信号(替代时间),次数  = %u\r\n",++SyncCnt);
					HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
					HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
				}
			}
	}
#endif
  /* USER CODE END Event_Sync_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

