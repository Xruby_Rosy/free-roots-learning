/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include"KEY.h"
#include"usart.h"
#include<stdio.h>
#include<string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern volatile uint32_t CPU_RunTime;
uint8_t myCountingSem_ucMessagesWaiting = 20;
/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId LED_BHandle;
osThreadId LED_GHandle;
osThreadId LED_RHandle;
osThreadId KEYHandle;
osThreadId DisplayHandle;
osSemaphoreId myBinarySem01Handle;
osSemaphoreId myCountingSem01Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void LED_B_Task(void const * argument);
void LED_G_Task(void const * argument);
void LED_R_Task(void const * argument);
void KEY_Task(void const * argument);
void Display_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
	CPU_RunTime = 0;
}

__weak unsigned long getRunTimeCounterValue(void)
{
return CPU_RunTime;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of myBinarySem01 */
  osSemaphoreDef(myBinarySem01);
  myBinarySem01Handle = osSemaphoreCreate(osSemaphore(myBinarySem01), 1);

  /* definition and creation of myCountingSem01 */
  osSemaphoreDef(myCountingSem01);
  myCountingSem01Handle = osSemaphoreCreate(osSemaphore(myCountingSem01), 20);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  if(myCountingSem01Handle==NULL)
  	  HAL_UART_Transmit(&huart2, (uint8_t*)"创建计数信号量失败！ \r\n",16, HAL_MAX_DELAY);
    else
  	  HAL_UART_Transmit(&huart2, (uint8_t*)"创建计数信号量成功！ \r\n",16, HAL_MAX_DELAY);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of LED_B */
  osThreadDef(LED_B, LED_B_Task, osPriorityBelowNormal, 0, 128);
  LED_BHandle = osThreadCreate(osThread(LED_B), NULL);

  /* definition and creation of LED_G */
  osThreadDef(LED_G, LED_G_Task, osPriorityBelowNormal, 0, 128);
  LED_GHandle = osThreadCreate(osThread(LED_G), NULL);

  /* definition and creation of LED_R */
  osThreadDef(LED_R, LED_R_Task, osPriorityBelowNormal, 0, 128);
  LED_RHandle = osThreadCreate(osThread(LED_R), NULL);

  /* definition and creation of KEY */
  osThreadDef(KEY, KEY_Task, osPriorityAboveNormal, 0, 256);
  KEYHandle = osThreadCreate(osThread(KEY), NULL);

  /* definition and creation of Display */
  osThreadDef(Display, Display_Task, osPriorityNormal, 0, 128);
  DisplayHandle = osThreadCreate(osThread(Display), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
    HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_LED_B_Task */
/**
* @brief Function implementing the LED_B thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_B_Task */
void LED_B_Task(void const * argument)
{
  /* USER CODE BEGIN LED_B_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END LED_B_Task */
}

/* USER CODE BEGIN Header_LED_G_Task */
/**
* @brief Function implementing the LED_G thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_G_Task */
void LED_G_Task(void const * argument)
{
  /* USER CODE BEGIN LED_G_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END LED_G_Task */
}

/* USER CODE BEGIN Header_LED_R_Task */
/**
* @brief Function implementing the LED_R thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LED_R_Task */
void LED_R_Task(void const * argument)
{
  /* USER CODE BEGIN LED_R_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END LED_R_Task */
}

/* USER CODE BEGIN Header_KEY_Task */
/**
* @brief Function implementing the KEY thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_KEY_Task */
void KEY_Task(void const * argument)
{
  /* USER CODE BEGIN KEY_Task */
	BaseType_t xResult;
	char buff[100];
  /* Infinite loop */
  for(;;)
  {
	  //按键检测
	  KEY.GetKeyCode();
	  //KEY1按下,获取信号量
	  if(KEY.KeyCode==KEY1)
	  {
		sprintf(buff,"%s \r\n","获取计数信号量，模拟车辆入库，申请停车位");
		HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		xResult = xSemaphoreTake(myCountingSem01Handle,0);
		if(xResult == pdTRUE)
		{
			sprintf(buff,"%s \r\n","获取成功，成功申请停车位，发送同步显示信号");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
			myCountingSem_ucMessagesWaiting--;
			sprintf(buff,"停车位剩余：%d \r\n",myCountingSem_ucMessagesWaiting);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
		else
		{
			sprintf(buff,"%s \r\n","获取失败，停车位已满");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		}
	  }
	  //KRY2按下，释放信号量
	  if(KEY.KeyCode==KEY2)
	  {
			sprintf(buff,"%s \r\n","释放计数信号量，模拟车辆出库，让出停车位");
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
		  xResult = xSemaphoreGive(myCountingSem01Handle);

			if(xResult == pdTRUE)
			{
				sprintf(buff,"%s \r\n","释放成功，成功让出停车位，发送同步显示信号");
				HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
				myCountingSem_ucMessagesWaiting++;

			}
			else
			{
				sprintf(buff,"%s \r\n","释放失败，停车位已空");
				HAL_UART_Transmit(&huart2, (uint8_t*)buff,strlen(buff), HAL_MAX_DELAY);
			}
	  }

	 osDelay(20);
  }
  /* USER CODE END KEY_Task */
}

/* USER CODE BEGIN Header_Display_Task */
/**
* @brief Function implementing the Display thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Display_Task */
void Display_Task(void const * argument)
{
  /* USER CODE BEGIN Display_Task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END Display_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

